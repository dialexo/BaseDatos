﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.IO;

namespace BaseDatos.Droid
{
    [Activity(Label = "BaseDatos", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            //creamos unas clases para identificar dnde estan guardadas las bases (con el nombre del archivo de mi base de datos)

            string name_file_db = "tareas.sqlite";

            // la ruta donde se encuentra en android

            string path_android = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            // concatenar con la base de datos al .Combine() le asigno unos parametros donde path_db tiene toda la ruta de la url

            string path_db = Path.Combine(path_android, name_file_db);

            // le paso la ruta PPal como parametro

            LoadApplication(new App(path_db));
        }
    }
}

