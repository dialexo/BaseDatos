﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace BaseDatos
{
	public partial class App : Application
	{
       
        // creamos un atributo adicional de clase

        public static string urlBd;

        // este metodo es un constructor por que tiene el mismo nombre de la clase

        public App ()
		{
			InitializeComponent();

			MainPage = new BaseDatos.MainPage();
		}

        // sobre escribimos el metodo constructor y le asignamos parametro url de donde se ubica la base de datos

        public App(string url)
        {
            InitializeComponent();

            // llamo el metodo le paso la url y la tiene global la siguiente opcion

            urlBd = url;

            MainPage = new BaseDatos.MainPage();
        }

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
