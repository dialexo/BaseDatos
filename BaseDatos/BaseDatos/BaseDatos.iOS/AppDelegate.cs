﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Foundation;
using UIKit;

namespace BaseDatos.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            //pegamos las mismas lineas de android

            string name_file_db = "tareas.sqlite";

            // la ruta donde se encuentra en android en esta ventana cambiaomos el nombre

            string path_ios = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            // creamos otra variable, para sacar de la carpeta deonde se encontraba e ingrese a la nueva

            string path_ios_final = Path.Combine(path_ios, "..", "Library");

            // concatenar con la base de datos al .Combine() le asigno unos parametros donde path_db tiene toda la ruta de la url
            // cambiamos el parametro

            string path_db = Path.Combine(path_ios_final, name_file_db);

            //le asignamos el nuevo parametro

            LoadApplication(new App(path_db));

            return base.FinishedLaunching(app, options);
        }
    }
}
